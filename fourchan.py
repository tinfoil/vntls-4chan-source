from __future__ import print_function

try:
    from urllib.request import urlopen, install_opener, build_opener
    from html.parser import HTMLParser
    from html.entities import entitydefs
    from urllib.parse import unquote
except ImportError:
    from urllib2 import install_opener, build_opener
    from urllib import urlopen
    from HTMLParser import HTMLParser
    from htmlentitydefs import entitydefs
    from urllib import unquote

    chr = unichr

import json
import logging
import re

RE_STATUS = re.compile(r'^>?(?P<name>[\w\d].+?)(?:\s*[:\-]\s+|\s+[:\-]\s*)(?P<update_text>[~"\'\w\d].+?)$')
RE_PERCENT = re.compile(r'(?P<percent>\d+(?:\.\d+)?)%.+?(?P<type>translated|edited|complete|partial patch)')
RE_STARTING_PERCENT = re.compile(r'^(?P<percent>\d+(?:\.\d+)?)%')
RE_PROPORTION = re.compile(r'(?P<numerator>\d+(?:\.\d+)?)/(?P<denominator>\d+(?:\.\d+)?).+?(?P<type>translated|edited)')

# be nice and use a user-agent
_opener = build_opener()
_opener.addheaders = (
    (('User-Agent', 'VNTLS 4chan/1.0 (https://bitbucket.org/tinfoil/vntls-4chan-source'), )
)
install_opener(_opener)


#noinspection PyAttributeOutsideInit
class UpdateParser(HTMLParser):
    """
    Big ol' state machine for parsing update HTML.

    Results are accessible at UpdateParser.statuses after it runs.
    """

    def __init__(self):
        HTMLParser.__init__(self)
        self.reset()

    def handle_starttag(self, tag, attrs):
        # greentext'd updates
        if tag == 'span' and ('class', 'quote') in attrs:
            self.in_quote = True

        elif tag == 'br':
            # parse and guess at update
            if self.data:
                match = re.match(RE_STATUS, self.data)

                if match:
                    name, status = match.group('name', 'update_text')
                    self.statuses[name] = {u'status': status, u'update': self.in_quote}
                    self.in_quote = False
                    self.last_name = name

                # no match, so it's not data
                else:
                    logging.debug('No match for: {0}'.format(self.data))

                # clear data
                self.data = ''

    def handle_data(self, data):
        content = unquote(data)
        self.data += data

    def handle_entityref(self, name):
        self.data += entitydefs[name]

    def handle_charref(self, name):
        # a decode/value exception here means something is actually wrong
        self.data += chr(int(name))

    def handle_endtag(self, tag):
        if tag == 'br' and self.in_quote:
            self.in_quote = False

    def reset(self):
        HTMLParser.reset(self)

        # custom state variables
        self.in_quote = False
        self.statuses = {}
        self.current = None

        self.data = ''
        self.in_quote = False
        self.hit_entity = False
        self.last_name = None


class UpdateClient(object):
    """
    4chan update client for the VNTLS.
    """
    api_fmt = 'https://a.4cdn.org/{board}/res/{thread}.json'
    img_fmt = 'https://i.4cdn.org/{board}/src/{timestamp}{ext}'

    def __init__(self, username, trip, thread_num, board='jp', now=False):
        self.username = username
        self.trip = trip
        self.thread_num = thread_num
        self.board = board

        self.url = self.api_fmt.format(board=board, thread=thread_num)
        if now:
            self.get_json()
        else:
            self.jdata = None

    def get_json(self):
        logging.debug('Getting {board} thread {thread}'.format(board=self.board, thread=self.thread_num))
        resp = urlopen(self.url).read().decode('utf8')
        self.jdata = json.loads(resp)
        return self.jdata

    def get_statuses(self):
        """
        Parse statuses and image from a VNTS-like source.

        :return: dict of updates
        """
        updates = {}
        parser = UpdateParser()
        if not self.jdata:
            self.jdata = self.get_json()
        for post in self.jdata['posts']:
            # parse only valid updates
            name_valid = 'name' in post and self.username == post['name'] if self.username else True
            trip_valid = 'trip' in post and self.trip == post['trip'] if self.trip else True
            if name_valid and trip_valid:
                # feed the output to the parser
                parser.feed(post['com'])
                updates.update(parser.statuses)

                # reset state for the next run
                parser.reset()

        # make numbers from updates
        for name, data in updates.items():
            # skip special fields
            if name.startswith('vntls'):
                continue
            status_to_nums(data)
        return updates

    def get_header(self):
        if not self.jdata:
            self.jdata = self.get_json()

        # get first image of valid posts
        for post in self.jdata['posts']:
            if post['name'] == self.username and post['trip'] == self.trip:
                filename = '{0}{1}'.format(post['tim'], post['ext'])
                url = self.img_fmt.format(board=self.board, timestamp=post['tim'], ext=post['ext'])
                return filename, url


def get_json(username, trip, thread, board='jp'):
    """
    Shortcut for UpdateClient().get_json()

    :param username: updater's username
    :param trip: updater's tripcode
    :param thread: thread number
    :param board: thread board
    :return: parsed JSON
    """
    upc = UpdateClient(username, trip, thread, board)
    return upc.get_json()


def parse_statuses(username, trip, jdata):
    """
    Parses JSON data as a 4chan thread API result.

    :param username: updater's username
    :param trip: updater's tripcode
    :param jdata: JSON data
    :return: list of statuses
    """
    upc = UpdateClient(username, trip, 0, now=False)
    upc.jdata = jdata
    return upc.get_statuses()


def get_statuses(username, trip, thread, board='jp', offline_file=None):
    """
    Get 4chan status for a given thread. Presence of file-like object `offline_file` will make this parse offline JSON.

    :param username: Trusted updater username
    :param trip: Trusted updater tripcode
    :param thread: Thread number
    :param board: 4chan board
    :param offline_file: handler to JSON file
    :return:
    """
    upc = UpdateClient(username, trip, thread, board, now=bool(offline_file is None))

    # feed in offline data, if applicable
    if offline_file:
        upc.jdata = json.load(offline_file)
    return upc.get_statuses()


def status_to_nums(scrape_data):
    """
    Resolves textual statuses to numbers using heuristic regular expression guesses.
    Updates `scrape_data` with `translated` and `edited` keys, if applicable.

    :param scrape_data: scraped project data
    """

    numstatuses = {}
    status = scrape_data['status']
    for percent, cls in re.findall(RE_PERCENT, status):
        # don't handle the "x translated, y translated" case -- other parts will correct
        numstatuses[cls] = float(percent) / 100

    for numerator, denominator, cls in re.findall(RE_PROPORTION, status):
        # percents take priority
        if cls in numstatuses:
            continue
        else:
            numstatuses[cls] = float(numerator) / float(denominator)

    if not 'translated' in numstatuses:
        desperation = re.match(RE_STARTING_PERCENT, status)
        if desperation:
            numstatuses['translated'] = float(desperation.group(1)) / 100

    scrape_data.update(numstatuses)


def test():
    from pprint import pprint

    print('Running light update parser test:')
    uc = UpdateClient('VNTS', '!!UcDDlzwANFs', 11988948, now=False)
    header = uc.get_header()
    statuses = uc.get_statuses()
    pprint(header)
    pprint(statuses)


if __name__ == '__main__':
    test()